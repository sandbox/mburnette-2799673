INTRODUCTION
------------

The Drupal Status module creates a bridge between Bespin.cc and 
your websites. With this module enabled, a key will be generated 
that Bespin.cc uses to authenticate retrieving a JSON object of 
your site's installed modules and their versions.

From there, Bespin.cc will notify you of any available updates 
to your site (including Drupal core and contributed modules) and 
any security releases.

Bespin.cc aims to help you keep your sites up-to-date with a 
dashboard to see all your sites at a glance.


REQUIREMENTS
------------

To use this module, you'll need to sign up for a free Bespin 
account (https://bespin.cc).


INSTALLATION
------------

Install as you would normally install a contributed Drupal module.


CONFIGURATION
-------------

Get your site's Bespin API key at Administration » Configuration » 
System » Drupal Status
