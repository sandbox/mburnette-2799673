<?php

/**
 * @file
 * Admin for drupalstatus.
 */

/**
 * Page callback: System settings settings.
 *
 * @see system_status_menu()
 */
function drupalstatus_form($form, &$form_state) {

  $form['drupalstatus_siteprotocol'] = array(
    '#type' => 'textfield',
    '#title' => t('Your site protocol'),
    '#description' => str_replace($GLOBALS['_SERVER']['HTTP_HOST'], '', $GLOBALS['base_url']),
    '#default_value' => str_replace($GLOBALS['_SERVER']['HTTP_HOST'], '', $GLOBALS['base_url']),
    '#attributes' => array('style' => array('display:none;')),
    '#size' => 60,
    '#maxlength' => 60,
    '#disabled' => TRUE,
  );

  $form['drupalstatus_siteurl'] = array(
    '#type' => 'textfield',
    '#title' => t('Your site url'),
    '#description' => $GLOBALS['_SERVER']['HTTP_HOST'],
    '#default_value' => $GLOBALS['_SERVER']['HTTP_HOST'],
    '#attributes' => array('style' => array('display:none;')),
    '#size' => 60,
    '#maxlength' => 60,
    '#disabled' => TRUE,
  );

  $form['drupalstatus_sitekey'] = array(
    '#type' => 'textfield',
    '#title' => t('Your site key'),
    '#description' => drupalstatus_get_sitekey(),
    '#default_value' => drupalstatus_get_sitekey(),
    '#attributes' => array('style' => array('display:none;')),
    '#size' => 60,
    '#maxlength' => 60,
    '#disabled' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Custom get_sitekey() function.
 */
function drupalstatus_get_sitekey() {
  $sitekey = variable_get('drupalstatus_key', 'Err-no-key');
  return $sitekey;
}
